﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Resources;
using System.Reflection;
using Hw20.Properties;

namespace Hw20
{
    public partial class CardGame : Form
    {
        int myCardValue = 0;
        int opponentCardValue = 0;
        int myScore = 0;
        int opponentScore = 0;
        Object previousSender = null;
        NetworkStream publicClientStream;
        int exitFlag = 0;
        public CardGame()
        {
            InitializeComponent();
            Thread server = new Thread(connectionWithServer);
            server.IsBackground = true;
            server.Start();
        }

        public void connectionWithServer()
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Hw20.Properties.Resources", typeof(Resources).Assembly);
            this.Forfeit_Button.Enabled = false;
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();
                publicClientStream = clientStream;
                byte[] bufferIn = new byte[4];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);
                Invoke((MethodInvoker)delegate { this.Forfeit_Button.Enabled = true; GenerateCards(clientStream);});
                while(input[0] != '2')
                {
                    bytesRead = clientStream.Read(bufferIn, 0, 4);
                    input = new ASCIIEncoding().GetString(bufferIn);
                    opponentCardValue = findValue(input);
                    
                    while (myCardValue == 0);
                    if (rm.GetObject(findImage(input)) != null)
                    {
                        this.blue_card.Image = (Image)(rm.GetObject(findImage(input)));
                        if (myCardValue > opponentCardValue)
                            myScore++;
                        else if (opponentCardValue > myCardValue)
                            opponentScore++;
                    }
                    Invoke((MethodInvoker)delegate { label1.Text = "Your score: " + myScore.ToString(); label2.Text = "Oponent's score: " + opponentScore.ToString(); });
                    myCardValue = 0;
                    opponentCardValue = 0;
                }
                MessageBox.Show(this, "Your opoonent forfeited\n Your score: " + myScore.ToString() + "\nOponent's score: " + opponentScore.ToString());
                exitFlag = 1;
                Application.Exit(); 
            }
            catch(System.Net.Sockets.SocketException)
            {
                return;
            }
        }

        public void GenerateCards(NetworkStream clientStream)
        {
            System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Hw20.Properties.Resources", typeof(Resources).Assembly);
            int cardNumber = 0;
            int cardType = 0;
            this.WindowState = FormWindowState.Maximized;
            int i = 0;
            string sendMessage;
            this.BackColor = Color.Green;
            Point nextLocation = new Point(90, 530);
            for (i = 1; i <= 10; i++)
            {
                System.Windows.Forms.PictureBox red_card = new PictureBox();
                red_card.Name = "red_back_card" + i;
                red_card.Image = global::Hw20.Properties.Resources.card_back_red;
                red_card.Location = nextLocation;
                red_card.Size = new System.Drawing.Size(110, 150);
                red_card.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                red_card.Click += delegate(Object sender1, EventArgs e1)
                {
                    Random rnd = new Random();
                    if (previousSender != null)
                        ((PictureBox)previousSender).Image = (Image)(rm.GetObject("card_back_red"));
                    cardNumber = rnd.Next(1, 14);
                    if (cardNumber < 10)
                        sendMessage = "10" + cardNumber.ToString();
                    else
                        sendMessage = "1" + cardNumber.ToString();
                    cardType = rnd.Next(1, 5);
                    if (cardType == 1)
                        sendMessage += "H";
                    else if (cardType == 2)
                        sendMessage += "C";
                    else if (cardType == 3)
                        sendMessage += "S";
                    else
                        sendMessage += "D";
                    myCardValue = findValue(sendMessage);
                    byte[] buffer = new ASCIIEncoding().GetBytes(sendMessage);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();
                    ((PictureBox)sender1).Image = (Image)(rm.GetObject(findImage(sendMessage)));
                    if (opponentCardValue == 0)
                        this.blue_card.Image = (Image)(rm.GetObject("card_back_blue"));
                    previousSender = sender1;
                };
                this.Controls.Add(red_card);
                nextLocation.X += 120;
            }
        }

        private void Forfeit_Button_Click(object sender, EventArgs e)
        {
            if (exitFlag == 0)
            {
                MessageBox.Show("Your score: " + myScore.ToString() + "\nOponent's score: " + opponentScore.ToString());
                byte[] buffer = new ASCIIEncoding().GetBytes("2000");
                publicClientStream.Write(buffer, 0, 4);
                publicClientStream.Flush();
                exitFlag = 1;
                Application.Exit();
            }
        }

        public string findImage(string input)
        {
            string returnStr;
            string number = input[1].ToString() + input[2].ToString();
            if (number[0] == '0')
            {
                if (input[2] == '1')
                    number = "ace";
                else
                    number = "_" + input[2].ToString();
            }
            else if (number[1] == '0')
                number = "_10";
            else if (number[1] == '1')
                number = "jack";
            else if (number[1] == '2')
                number = "queen";
            else
                number = "king";
            string type = input[3].ToString();
            returnStr = number + "_of_";
            if (type == "C")
                returnStr += "clubs";
            else if (type == "D")
                returnStr += "diamonds";
            else if (type == "S")
                returnStr += "spades";
            else
                returnStr += "hearts";
            return returnStr;
        }

        public int findValue(string input)
        {
            string number = input[1].ToString() + input[2].ToString();
            if (number[0] == '0')
                return int.Parse(input[2].ToString());
            else
                return int.Parse(number);
        }
    }
}
